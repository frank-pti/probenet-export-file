using Frank.Widgets;
using Frank.Widgets.Dialogs;
using Internationalization;
/*
 * The base library for ProbeNet file exports
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;

namespace ProbeNet.Export.File
{
    /// <summary>
    /// Base class for file exports.
    /// </summary>
    public abstract class FileExport : DataExport
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Export.File.FileExport"/> class.
        /// </summary>
        /// <param name="settingsProvider">Settings provider.</param>
        public FileExport(SettingsProvider settingsProvider) :
            base(settingsProvider)
        {
        }

        /// <summary>
        /// Gets the dialog.
        /// </summary>
        /// <returns>The dialog.</returns>
        /// <param name="i18n">I18n.</param>
        /// <param name="parent">Parent window.</param>
        /// <param name="filters">File filters.</param>
        /// <param name="curveDataAvailable">If set to <c>true</c> curve data is available.</param>
        /// <param name="includeCurveData">If set to <c>true</c> include curve data.</param>
        /// <param name="openAfterSave">If set to <c>true</c> open after save.</param>
        public static IExportDialog GetDialog(
            I18n i18n,
            Gtk.Window parent,
            IList<FileFilter> filters,
            bool curveDataAvailable,
            bool includeCurveData,
            bool openAfterSave)
        {
            if (Frank.Widgets.Helper.WindowsDetected) {
                return new WindowsExportDialog(i18n, filters, curveDataAvailable, false) {
                    CanExportCurve = curveDataAvailable
                };
            }
            return new GtkExportDialog(i18n, parent, filters, includeCurveData, openAfterSave) {
                CanExportCurve = curveDataAvailable
            };
        }

        /// <summary>
        /// Export the data using the specified i18n, model, measurement ids to a file with the specified name.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="model">Model.</param>
        /// <param name="measurementIdsForExport">Measurement identifiers for export.</param>
        /// <param name="filename">Filename.</param>
        public abstract void Export(
            I18n i18n, MultipleSourcesDataModelBase model, IList<IList<string>> measurementIdsForExport, string filename);

        /// <summary>
        /// Gets the file filter extension.
        /// </summary>
        /// <returns>The file filter extension.</returns>
        public string GetFileFilterExtension()
        {
            Type type = this.GetType();
            object[] attributes = type.GetCustomAttributes(typeof(FileFilterAttribute), true);
            if (attributes.Length > 0) {
                return (attributes[0] as FileFilterAttribute).Extension;
            }
            return String.Empty;
        }

        /// <summary>
        /// Gets the export settings dialog.
        /// </summary>
        /// <returns>The export settings dialog.</returns>
        /// <param name="parent">Parent.</param>
        /// <param name="i18n">I18n.</param>
        public virtual Gtk.Dialog GetExportSettingsDialog(Gtk.Window parent, I18n i18n)
        {
            return null;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="ProbeNet.Export.File.FileExport"/> open after save.
        /// </summary>
        /// <value><c>true</c> if open after save; otherwise, <c>false</c>.</value>
        public virtual bool OpenAfterSave
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the name of the company.
        /// </summary>
        /// <value>The name of the company.</value>
        public string CompanyName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the company place.
        /// </summary>
        /// <value>The company place.</value>
        public string CompanyPlace
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the name of the program.
        /// </summary>
        /// <value>The name of the program.</value>
        public string ProgramName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the program version.
        /// </summary>
        /// <value>The program version.</value>
        public string ProgramVersion
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the result orientation.
        /// </summary>
        /// <value>The result orientation.</value>
        public Orientation ResultOrientation
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the result numbering format.
        /// </summary>
        /// <value>The result numbering format.</value>
        public ResultNumberingFormat ResultNumberingFormat
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the standard deviation decimal places.
        /// </summary>
        /// <value>The standard deviation decimal places.</value>
        public Nullable<int> StandardDeviationDecimalPlaces
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the statistics display formats.
        /// </summary>
        /// <value>The statistics display formats.</value>
        public IDictionary<string, NumericValueDisplayFormat> StatisticsDisplayFormats
        {
            get;
            set;
        }
    }
}

