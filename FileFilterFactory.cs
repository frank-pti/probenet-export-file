/*
 * The base library for ProbeNet file exports
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using Internationalization;
using System;
using System.Collections.Generic;
using System.Reflection;
using Frank.Widgets;

namespace ProbeNet.Export.File
{
    /// <summary>
    /// Factory class for file filters.
    /// </summary>
	public static class FileFilterFactory
	{
        /// <summary>
        /// Produces the file filters from enum using the <see cref="FileFilterAttribute"/> attribute of the elements.
        /// </summary>
        /// <returns>The file filters.</returns>
        /// <param name="i18n">I18n.</param>
        /// <param name="enumType">Enum type.</param>
        /// <param name="includeExperimental">If set to <c>true</c> include experimental. Experimental elements
        /// must have the <see cref="ExperimentalAttribute"/> attribute.</param>
		public static FileFilter[] ProduceFileFiltersFromEnum(I18n i18n, Type enumType, bool includeExperimental)
		{
			if (!enumType.IsEnum) {
				throw new ArgumentException("Type is not an enum", "enumType");
			}
			List<FileFilter> result = new List<FileFilter>();
			foreach (string name in Enum.GetNames(enumType)) {
				FieldInfo field = enumType.GetField(name);
				object[] captions = field.GetCustomAttributes(typeof(TranslatableCaptionAttribute), true);
				object[] experimental = field.GetCustomAttributes(typeof(ExperimentalAttribute), true);
                if (captions.Length == 0) {
                    throw new ArgumentException(
                        String.Format("The {0} value of the enum type {1} is missing the {2} attribute",
                            field.Name, enumType.Name, typeof(TranslatableCaptionAttribute).Name),
                        "enumType");
                }
    			TranslationString caption = (captions[0] as TranslatableCaptionAttribute).GetTranslation(i18n);
				if (includeExperimental || experimental == null || experimental.Length == 0) {
					object[] attributes = field.GetCustomAttributes(typeof(FileFilterAttribute), true);
					FileFilter filter = null;
					foreach (object attribute in attributes) {
						FileFilterAttribute fileFilterAttribute = attribute as FileFilterAttribute;
						filter = fileFilterAttribute.ProduceFileFilter(caption, filter);
					}
					result.Add(filter);
				}
			}
			return result.ToArray();
		}

        /// <summary>
        /// Produces the file filters from export list.
        /// </summary>
        /// <returns>The file filters from export list.</returns>
        /// <param name="i18n">I18n.</param>
        /// <param name="exports">Exports.</param>
        /// <param name="includeExperimental">If set to <c>true</c> include experimental.</param>
        public static Dictionary<FileFilter, IExportInformation> ProduceFileFiltersFromExportList(I18n i18n, IList<IExportInformation> exports, bool includeExperimental)
        {
            Dictionary<FileFilter, IExportInformation> filters = new Dictionary<FileFilter, IExportInformation>();
            foreach (IExportInformation export in exports) {
                if (export.Type.IsSubclassOf(typeof(FileExport))) {
                    Type type = export.Type;
                    object[] experimental = type.GetCustomAttributes(typeof(ExperimentalAttribute), true);
                    TranslationString caption = export.GetCaption(i18n);
                    if (includeExperimental || experimental == null || experimental.Length == 0) {
                        object[] attributes = type.GetCustomAttributes(typeof(FileFilterAttribute), true);
                        FileFilter filter = null;
                        foreach (object attribute in attributes) {
                            FileFilterAttribute fileFilterAttribute = attribute as FileFilterAttribute;
                            filter = fileFilterAttribute.ProduceFileFilter(caption, filter);
                        }
                        filters.Add(filter, export);
                    }
                }
            }
            return filters;
        }
	}
}
