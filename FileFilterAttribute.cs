/*
 * The base library for ProbeNet file exports
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using Internationalization;
using Frank.Widgets;

namespace ProbeNet.Export.File
{
    /// <summary>
    /// File filter attribute.
    /// </summary>
	[AttributeUsage(AttributeTargets.Field | AttributeTargets.Class, AllowMultiple = true)]
	public class FileFilterAttribute: Attribute
	{
		private string extension;

        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Export.File.FileFilterAttribute"/> class.
        /// </summary>
        /// <param name="extension">Extension.</param>
        /// <param name="mimeType">MIME type.</param>
        /// <param name="includeExtensionInName">If set to <c>true</c> include extension in name.</param>
		public FileFilterAttribute (string extension, string mimeType, bool includeExtensionInName = true)
		{
			this.extension = extension;
			this.MimeType = mimeType;
			this.IncludeExtensionInName = includeExtensionInName;
		}

        /// <summary>
        /// Gets the extension.
        /// </summary>
        /// <value>The extension.</value>
		public string Extension {
			get {
				return extension;
			}
		}

        /// <summary>
        /// Gets or sets the type of the MIME.
        /// </summary>
        /// <value>The type of the MIME.</value>
		public string MimeType {
            get;
            set;
        }

        /// <summary>
        /// Gets the pattern. If the extension is "txt" this property will return "*.txt".
        /// </summary>
        /// <value>The pattern.</value>
		public string Pattern {
			get {
				return String.Format("*.{0}", Extension);
			}
		}

        /// <summary>
        /// Gets the file name suffix. If the extension is "txt" this property will return ".txt".
        /// </summary>
        /// <value>The file name suffix.</value>
		public string FileNameSuffix {
			get {
				return String.Format(".{0}", Extension);
			}
		}

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="ProbeNet.Export.File.FileFilterAttribute"/> include
        /// extension in name.
        /// </summary>
        /// <value><c>true</c> if include extension in name; otherwise, <c>false</c>.</value>
		public bool IncludeExtensionInName {
            get;
            set;
        }

        /// <summary>
        /// Produces the file filter.
        /// </summary>
        /// <returns>The file filter.</returns>
        /// <param name="description">Description.</param>
        /// <param name="filter">Filter.</param>
		public FileFilter ProduceFileFilter(TranslationString description, FileFilter filter = null)
		{
			return ProduceFileFilter(this, description, filter);
		}

        /// <summary>
        /// Produces the file filter.
        /// </summary>
        /// <returns>The file filter.</returns>
        /// <param name="attribute">Attribute.</param>
        /// <param name="description">Description.</param>
        /// <param name="filter">Filter.</param>
		public static FileFilter ProduceFileFilter(FileFilterAttribute attribute, TranslationString description, FileFilter filter = null)
		{
			if (filter == null) {
				filter = new FileFilter(attribute.Extension, attribute.MimeType);
			} else {
                filter.AddExtension(attribute.Extension);
				filter.AddMimeType(attribute.MimeType);
				filter.AddPattern(attribute.Pattern);
			}
			if (attribute.IncludeExtensionInName) {
				filter.Name = String.Format("{0} ({1})", description.Tr, attribute.Pattern);
			} else {
				filter.Name = description.Tr;
			}
			return filter;
		}
	}
}

