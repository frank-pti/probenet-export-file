using Internationalization;
using Logging;
/*
 * The base library for ProbeNet file exports
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;

namespace ProbeNet.Export.File
{
    /// <summary>
    /// Combo box that displays a list of file exports.
    /// </summary>
    public class FileExportComboBox : DataExportComboBox
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProbeNet.Export.File.FileExportComboBox"/> class.
        /// </summary>
        /// <param name="i18n">I18n.</param>
        /// <param name="exportList">Export list.</param>
        public FileExportComboBox(I18n i18n, List<IExportInformation> exportList) :
            base(i18n, exportList)
        {
        }

        /// <inheritdoc/>
        protected override string BuildDataString(IExportInformation dataExport, object[] renderParameters)
        {
            try {
                string dataString = base.BuildDataString(dataExport, renderParameters);
                string pattern = String.Empty;
                FileFilterAttribute[] fileFilters =
                    dataExport.GetType().GetCustomAttributes(typeof(FileFilterAttribute), true) as FileFilterAttribute[];
                if (fileFilters.Length > 0) {
                    pattern = String.Format("({0})", fileFilters[0].Pattern);
                }
                return String.Format("{0} {1}", dataString, pattern).Trim();
            } catch (Exception e) {
                Logger.Log(e);
                if (e.InnerException != null) {
                    Logger.Log(e.InnerException);
                }
                return base.BuildDataString(dataExport, renderParameters);
            }
        }
    }
}

