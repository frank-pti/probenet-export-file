/*
 * The base library for ProbeNet file exports
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using System.Reflection;
using System.ComponentModel;

namespace ProbeNet.Export.File
{
    /// <summary>
    /// Helper class for the statistics methods.
    /// </summary>
    public static class StatisticsMethodHelper
    {
        /// <summary>
        /// Loads the statistics display formats.
        /// </summary>
        /// <returns>The statistics display formats.</returns>
        public static IDictionary<string, NumericValueDisplayFormat> LoadStatisticsDisplayFormats ()
        {
            IList<MethodInfo> methods = Statistics.GetMethods();
            IDictionary<string, NumericValueDisplayFormat> formats = new Dictionary<string, NumericValueDisplayFormat> ();
            
            foreach (MethodInfo method in methods) {
                DescriptionAttribute[] descriptionAttribute =
                    method.GetCustomAttributes(typeof(DescriptionAttribute), false) as DescriptionAttribute[];
                if (descriptionAttribute.Length == 0) {
                    throw new ArgumentException(
                        String.Format ("Statistics method {0} is missing the Description attribute", method.Name));
                }
                DefaultNumericValueDisplayFormatAttribute[] formatAttribute =
                    method.GetCustomAttributes(typeof(DefaultNumericValueDisplayFormatAttribute), false) as DefaultNumericValueDisplayFormatAttribute[];
                if (formatAttribute.Length == 0) {
                    throw new ArgumentException(
                        String.Format ("Statistics method {0} is missing the DefaultNumericValueDisplayFormat attribute", method.Name));
                }
                
                formats.Add(descriptionAttribute[0].Description, formatAttribute[0].Format);
            }
            
            return formats;
        }
    }
}

